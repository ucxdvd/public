#version=DEVEL
# System authorization information
auth --enableshadow --passalgo=sha512
# Use CDROM installation media
cdrom
# Use graphical install
graphical
# Run the Setup Agent on first boot
firstboot --enable
ignoredisk --only-use=sda,sdb
# Keyboard layouts
keyboard --vckeymap=us --xlayouts='us'
# System language
lang en_US.UTF-8

# Network information
network  --bootproto=static --device=ens192 --gateway=10.0.0.1 --ip=10.0.0.10 --nameserver=8.8.8.8 --netmask=255.255.255.0 --ipv6=auto --activate
network  --hostname=forekat.vir

# Root password
rootpw --iscrypted $6$nLmVUY4b8t33EC1R$dPgO3DA/2Nm/S0fgEHTTerO7bp7NBrYbHU.yeWZ.z2BedVcID46mK/KyVYG6Lp995TqXxBXhLo.f3T4IR4EiE/
# System services
services --disabled="chronyd"
# System timezone
timezone Europe/Brussels --isUtc --nontp
user --groups=wheel --name=dave --password=$6$sed4uhGyUVAdNNmP$RQbIT0sKVeaAaMvqYdBSLglEpGRU6WOrVEP0FMQL5fUBN6KN4./1B/rOsP03lXEi4O75R1h.vMSEJW0rNMDUI1 --iscrypted --gecos="dave"
# System bootloader configuration
bootloader --append=" crashkernel=auto" --location=mbr --boot-drive=sda
# Partition clearing information
clearpart --none --initlabel
# Disk partitioning information
part /boot --fstype="xfs" --size=1024
part pv.202 --fstype="lvmpv" --ondisk=sdb --size=204711 --encrypted
part pv.196 --fstype="lvmpv" --ondisk=sda --size=203687 --encrypted
volgroup centos --pesize=4096 pv.196 pv.202
logvol /  --fstype="xfs" --size=396288 --name=root --vgname=centos
logvol swap  --fstype="swap" --size=12096 --name=swap --vgname=centos

%packages
@^file-print-server-environment
@base
@compat-libraries
@core
@file-server
@print-server
@security-tools
@smart-card
@system-admin-tools
kexec-tools

%end

%addon com_redhat_kdump --enable --reserve-mb='auto'

%end

%anaconda
pwpolicy root --minlen=6 --minquality=1 --notstrict --nochanges --notempty
pwpolicy user --minlen=6 --minquality=1 --notstrict --nochanges --emptyok
pwpolicy luks --minlen=6 --minquality=1 --notstrict --nochanges --notempty
%end
