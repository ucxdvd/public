#version=RHEL8
ignoredisk --only-use=sda
# Partition clearing information
clearpart --none --initlabel
# Use graphical install
graphical
# Keyboard layouts
keyboard --vckeymap=us --xlayouts='us'
# System language
lang en_US.UTF-8

# Network information
network  --bootproto=dhcp --device=ens192 --ipv6=auto --activate
network  --hostname=rhel82.vir
repo --name="AppStream" --baseurl=file:///run/install/repo/AppStream
# Root password
rootpw --iscrypted $6$20EyOsPsTyKy6VZz$rmJec5nrvUvcsPTaKF6cWiPbyXl6k2mCyYiMbA8OjyrbAq7/ovBm0wZxXyCKUABasENntZUiui8b1C9iyTw3g1
# Run the Setup Agent on first boot
firstboot --enable
# Do not configure the X Window System
skipx
# System services
services --disabled="chronyd"
# System timezone
timezone Europe/Brussels --isUtc --nontp
user --groups=wheel --name=dave --password=$6$Qa.sEpv2DL.cCK5X$HSYKdpFv6ORlHK5Ew90tdHTIcTpzI4SY8yUrCEhWGsc00dZEd2Of7mAgZ.tI700wmk.hR9M369bZTX7pFozpg/ --iscrypted --gecos="dave"
# Disk partitioning information
part pv.236 --fstype="lvmpv" --ondisk=sda --size=39935
part /boot --fstype="xfs" --ondisk=sda --size=1024
volgroup rhel --pesize=4096 pv.236
logvol swap --fstype="swap" --size=2213 --name=swap --vgname=rhel
logvol / --fstype="xfs" --grow --size=1024 --name=root --vgname=rhel

%packages
@^server-product-environment
@container-management
@development
@guest-agents
@headless-management
@legacy-unix
@network-file-system-client
@performance
@remote-system-management
@security-tools
@smart-card
@system-tools
kexec-tools

%end

%addon com_redhat_kdump --enable --reserve-mb='auto'

%end

%anaconda
pwpolicy root --minlen=6 --minquality=1 --notstrict --nochanges --notempty
pwpolicy user --minlen=6 --minquality=1 --notstrict --nochanges --emptyok
pwpolicy luks --minlen=6 --minquality=1 --notstrict --nochanges --notempty
%end
