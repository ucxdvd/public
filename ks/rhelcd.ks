lang en_US
keyboard us
timezone Europe/Brussels
rootpw $1$bXVjBn/m$rqK3x7qdD/KoxUrwzW.eN0 --iscrypted
#platform x86, AMD64, or Intel EM64T
reboot
text
cdrom
bootloader --location=mbr --append="rhgb quiet crashkernel=auto"
zerombr
clearpart --all --initlabel
autopart
auth --passalgo=sha512 --useshadow
selinux --permissive
firewall --disabled
skipx
firstboot --disable
%packages
@^minimal-environment
@smart-card
@console-internet
%end