lvcreate -n containers -L 500M datavg
mkfs -t ext4 /dev/datavg/containers
mkdir /var/lib/containers
mount /dev/datavg/containers /var/lib/containers

yum install -y podman git screen tmux python3-pip
pip3 install podman-compose
