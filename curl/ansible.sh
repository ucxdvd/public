#!/bin/bash
set -x

function ubuntu() {
apt update
apt install software-properties-common
add-repository --yes --update ppa:ansible/ansible
apt install ansible
}

function debian() {
export DEBIAN_FRONTEND=noninteractive

echo "deb http://ppa.launchpad.net/ansible/ansible/ubuntu trusty main" > /etc/apt/sources.list.d/ansible.list
apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 93C4A3FD7BB9C367
apt-get update
apt-get install ansible
}

function centos() {
    yum install -y epel-release
    yum install -y ansible
}


function rhel8() {
    echo "Register this system before trying this"
    subscription-manager repos --enable ansible-2.9-for-rhel-8-x86_64-rpms
    yum install -y ansible
}

function rhel7() {
    echo "Register this system before trying this"
    subscription-manager repos --enable ansible-2.9-for-rhel-7-x86_64-rpms
    yum install -y ansible
}

$1
