lvcreate -n docker -L 500M datavg
mkfs -t ext4 /dev/datavg/docker
mkdir /var/lib/docker
mount /dev/datavg/docker /var/lib/docker
curl -L https://get.docker.com|sh -s
apt-get install docker-compose
yum install -y docker-compose
systemctl disable docker
systemctl start docker
