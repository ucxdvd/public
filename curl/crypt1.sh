#!/bin/bash
set -x

# disk 1, create vg, first fs
#PASSWORD=$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 13 ; echo '')
passfile=/run/fff
SIZE=10000
DEVICE=/disk1.img
CRYPT=crypt1
LUKSOPTS="--pbkdf pbkdf2"

dd if=/dev/zero of=/$DEVICE bs=1M seek=$SIZE count=1
#losetup -f /$DEVICE
#LOOP=$(losetup -j /$DEVICE|cut -f1 -d: )
#echo loopdev is $LOOP
echo YES |cryptsetup luksFormat $DEVICE $LUKSOPTS -d $passfile
cryptsetup luksOpen $DEVICE $CRYPT -d $passfile
#rm $passfile
vgextend datavg /dev/mapper/$CRYPT
