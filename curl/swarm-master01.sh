curl -L get.docker.com|sh -s
usermod -aG docker ggl
service docker restart
docker swarm init --advertise-addr $(curl ifconfig.co) 2>&1|tee /tmp/jointoken
docker swarm join-token manager 2>&1|tee /tmp/managertoken
