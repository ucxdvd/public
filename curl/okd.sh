#!/bin/bash
set -x
apt-get update

yum install epel-release

yum update -y
apt-get -y upgrade

curl -L get.docker.com|sh -s
yum install -y docker-compose ansible
apt-get install docker-compose ansible
usermod -aG docker dave
usermod -aG docker ggl
service docker restart
curl -L https://github.com/openshift/origin/releases/download/v3.11.0/openshift-origin-client-tools-v3.11.0-0cbc58b-linux-64bit.tar.gz|tar -C /usr/local/bin -xvzf -
cp /usr/local/bin/openshift*/oc /usr/local/bin /usr/local/sbin
cp /usr/local/bin/openshift*/kubectl /usr/local/bin /usr/local/sbin

oc cluster up --base-dir=/data/openshift --write-config=true --skip-registry-check=true
oc cluster up --base-dir=/data/openshift --skip-registry-check=true
oc cluster add router --base-dir=/data/openshift 
oc login -u system:admin
oc adm policy add-scc-to-user anyuid -z default
oc adm policy add-scc-to-user hostnetwork -z router
oc adm router roeteur

