#!/bin/bash
set -x
# debian
export DEBIAN_FRONTEND=noninteractive

apt-get update
apt-get install -y cryptsetup lvm2|tee /tmp/install.log
# redhat
yum install -y cryptsetup lvm2

# disk 0, create vg, first fs
PASSWORD=$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 13 ; echo '')
passfile=/run/fff
SIZE=4000
DEVICE=/disk0.img
CRYPT=crypt0
LUKSOPTS="--pbkdf pbkdf2"

echo $PASSWORD > $passfile
#cp $passfile /root/.fff
dd if=/dev/zero of=/$DEVICE bs=1M seek=$SIZE count=1
#losetup -f /$DEVICE
#LOOP=$(losetup -j /$DEVICE|cut -f1 -d: )
echo loopdev is $LOOP
echo YES |cryptsetup luksFormat $DEVICE $LUKSOPTS $passfile
cryptsetup luksOpen $DEVICE $CRYPT -d $passfile
#rm $passfile
pvcreate /dev/mapper/$CRYPT
vgcreate datavg /dev/mapper/$CRYPT
lvcreate -n data -L 500M datavg

mkfs -t ext4 /dev/datavg/data
mkdir /data
mount /dev/datavg/data /data

#mkfs -t ext4 /dev/datavg/docker
#mkdir /var/lib/docker
#mount /dev/datavg/docker /var/lib/docker

