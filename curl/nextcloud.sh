export SCRIPTBASE=https://gitlab.com/ucxdvd/public/-/raw/master/curl


# split into debian.sh
apt-get update
apt-get install -y tmux git screen ansible 
#apt-get upgrade -y

curl -L $SCRIPTBASE/crypt0.sh|sh -s |tee /var/run/crypt0.log
curl -L $SCRIPTBASE/crypt1.sh|sed s/10000/2000/g|sh -s |tee /var/run/crypt1.log
curl -L $SCRIPTBASE/docker.sh|sh -s |tee /var/run/docker.log

# what was this for again?
cd /data
git clone https://github.com/ucxdvd/binaries.git
cd binaries
export publicip=$(curl checkip.dyndns.org|cut -b77-90)
bash generate_ssl.sh $publicip

# split into webserver.sh ?
lvcreate -n www -L 200M datavg
mkfs -t ext4 /dev/datavg/www
mkdir /var/www
mount /dev/datavg/www /www
mkdir /var/www/html

#lvcreate -n mysql -L 200M datavg
#mkfs -t ext4 /dev/datavg/mysql
#mkdir /var/lib/mysql
#mount /dev/datavg/mysql var/lib/mysql
 #
 #docker run -d -v db:/var/lib/mysql mariadb:10.5
 docker run -d -v nextcloud:/var/www/html nextcloud
