#!/bin/bash
# assumed : libvirtd running, virt-install and genisoimage installed

mkdir /home/iso
wget -O /home/iso/centos7.qcow2 http://cloud.centos.org/centos/7/images/CentOS-7-x86_64-GenericCloud.qcow2

IMAGEDIR=/var/lib/libvirt/images ## <-- Dir to store new VM ##
IMAGE=centos7.qcow2
VM=$1 ## <-- set vm name ##


mkdir -vp $IMAGEDIR/$VM
# copy or convert image
dd if=/home/iso/$IMAGE of=$IMAGEDIR/$VM/$VM.qcow2 status=progress

genisoimage -o $IMAGEDIR/$VM/$VM-cidata.iso -V cidata -J -r user-data meta-data
virsh pool-create-as --name $VM --type dir --target $IMAGEDIR/$VM

virt-install --import --name $VM \
--memory 512 --vcpus 2 --cpu host \
--disk $IMAGEDIR/$VM/$VM.qcow2,format=qcow2,bus=virtio \
--disk $IMAGEDIR/$VM/$VM-cidata.iso,device=cdrom \
--network bridge=virbr0,model=virtio \
--os-type=linux \
--os-variant=centos7.0 \
--graphics spice \
--noautoconsole