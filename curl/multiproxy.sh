#!/usr/bin/bash
apt-get update
apt-get install -y tmux privoxy git screen
sed -i s/"listen-address  127.0.0.1:8118/listen-address  :8118"/g /etc/privoxy/config
curl -L https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts >> /etc/hosts
service privoxy restart
git clone https://github.com/ucxdvd/binaries.git
cd binaries
export publicip=$(curl checkip.dyndns.org|cut -b77-90)
bash generate_ssh.sh $publicip
screen ./ducklin-armhf -c ${publicip}-cert.pem -k ${publicip}-key.pem -a $publicip
